package uk.ac.aber.dcs.cs31620.wordmapp.datasource

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.wordmapp.model.Language
import uk.ac.aber.dcs.cs31620.wordmapp.model.LanguageDao
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslation
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslationDao

@Database(entities = [WordAndTranslation::class, Language::class], version = 1)
abstract class WordMappPersistentRoomDatabase : RoomDatabase(), RoomDatabaseInjected {

    abstract override fun wordAndTranslationDao(): WordAndTranslationDao
    abstract override fun languageDao(): LanguageDao

    override fun closeDb() {
        instance?.close()
        instance = null
    }

    companion object {
        private var instance: WordMappPersistentRoomDatabase? = null
        private val coroutineScope = CoroutineScope(Dispatchers.Main)

        fun getDatabase(context: Context): WordMappPersistentRoomDatabase? {
            synchronized(this) {
                if (instance == null) {
                    instance =
                        Room.databaseBuilder<WordMappPersistentRoomDatabase>(
                            context.applicationContext,
                            WordMappPersistentRoomDatabase::class.java,
                            "faa_database"
                        )
                            .allowMainThreadQueries() // Normally you would't but for testing
                            .addCallback(roomDatabaseCallback(context))
                            //.addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                            .build()
                }
                return instance!!
            }
        }

        private fun roomDatabaseCallback(context: Context): Callback {
            return object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    // Run in background thread
                    coroutineScope.launch(Dispatchers.IO) {
                        populateDatabase(context, getDatabase(context)!!)
                    }
                }
            }
        }

        private fun populateDatabase(context: Context, instance: WordMappPersistentRoomDatabase) {}
    }
}