package uk.ac.aber.dcs.cs31620.wordmapp.datasource

import android.content.Context

object Injection {
    fun getDatabase(context: Context): RoomDatabaseInjected =
        WordMappPersistentRoomDatabase.getDatabase(context)!!
}