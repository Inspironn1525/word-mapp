package uk.ac.aber.dcs.cs31620.wordmapp.datasource

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import uk.ac.aber.dcs.cs31620.wordmapp.model.Language
import uk.ac.aber.dcs.cs31620.wordmapp.model.LanguageDao
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslation
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslationDao

@Database(entities = [WordAndTranslation::class, Language::class], version = 2)
abstract class WordMappInMemoryRoomDatabase: RoomDatabase(), RoomDatabaseInjected{

    abstract override fun wordAndTranslationDao(): WordAndTranslationDao
    abstract override fun languageDao(): LanguageDao

    override fun closeDb() {
        instance?.close()
        instance = null
    }

    companion object {
        private var instance: WordMappInMemoryRoomDatabase? = null
        private val coroutineScope = CoroutineScope(Dispatchers.Main)

        fun getDatabase(context: Context): WordMappInMemoryRoomDatabase? {
            synchronized(this) {
                if (instance == null) {
                    instance =
                        Room.inMemoryDatabaseBuilder(
                            context.applicationContext,
                            WordMappInMemoryRoomDatabase::class.java
                        )
                            //.allowMainThreadQueries() // Normally you would't but for testing
                            .fallbackToDestructiveMigrationOnDowngrade()
                            .build()
                }
                return instance!!
            }
        }

    }
}