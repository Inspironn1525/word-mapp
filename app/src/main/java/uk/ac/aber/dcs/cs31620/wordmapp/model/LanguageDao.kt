package uk.ac.aber.dcs.cs31620.wordmapp.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface LanguageDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertLanguage(language: Language)

    @Query("DELETE FROM language")
    fun deleteAllLanguages()
}