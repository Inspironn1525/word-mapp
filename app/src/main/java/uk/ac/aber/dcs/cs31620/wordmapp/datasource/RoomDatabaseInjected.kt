package uk.ac.aber.dcs.cs31620.wordmapp.datasource

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import uk.ac.aber.dcs.cs31620.wordmapp.model.LanguageDao
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslation
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslationDao

interface RoomDatabaseInjected{

    fun languageDao(): LanguageDao
    fun wordAndTranslationDao(): WordAndTranslationDao
    fun closeDb()

//    companion object {
//        private var instance: RoomDatabaseInjected? = null
//        private val coroutineScope = CoroutineScope(Dispatchers.Main)
//
//        fun getDatabase(context: Context): RoomDatabaseInjected {
//            synchronized(this) {
//                if (instance == null) {
//                    instance =
//                        Room.databaseBuilder<RoomDatabaseInjected>(
//                            context.applicationContext,
//                            RoomDatabaseInjected::class.java,
//                            "wordmapp_database"
//                        )
//                            .allowMainThreadQueries()
//                            //.addCallback(roomDatabaseCallback(context))
//                            //.addMigrations(MIGRATION_1_2, MIGRATION_2_3)
//                            .build()
//                }
//                return instance!!
//            }
//        }
//    }
}