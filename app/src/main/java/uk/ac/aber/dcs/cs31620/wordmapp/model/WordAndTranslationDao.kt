package uk.ac.aber.dcs.cs31620.wordmapp.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface WordAndTranslationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertSingleWord(word: WordAndTranslation)

    @Insert
    fun insertMultipleWords(wordList: List<WordAndTranslation>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateWord(word: WordAndTranslation)

    @Delete
    fun deleteWord(word: WordAndTranslation)

    @Query("DELETE FROM word_and_translation")
    fun deleteAllWords()

    @Query("SELECT * FROM word_and_translation")
    fun getAllWords(): LiveData<List<WordAndTranslation>>


}