package uk.ac.aber.dcs.cs31620.wordmapp.datasource

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.ac.aber.dcs.cs31620.wordmapp.model.Language
import uk.ac.aber.dcs.cs31620.wordmapp.model.WordAndTranslation

class WordMappRepository(application: Application) {

    private val wordAndTranslationDao = Injection.getDatabase(application).wordAndTranslationDao()
    private val languageDao = Injection.getDatabase(application).languageDao()
    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    fun insertWord(word: WordAndTranslation){
        coroutineScope.launch(Dispatchers.IO){
            wordAndTranslationDao.insertSingleWord(word)
        }
    }

    fun insertLanguage(language: Language){
        coroutineScope.launch(Dispatchers.IO){
            languageDao.insertLanguage(language)
        }
    }

    fun getAllWords() = wordAndTranslationDao.getAllWords()
}