package uk.ac.aber.dcs.cs31620.wordmapp.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "language")
data class Language(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Int = 0,
    val nativeLang: String = "",
    val foreignLang: String = ""
    )