package uk.ac.aber.dcs.cs31620.wordmapp.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "word_and_translation")
data class WordAndTranslation (
        @PrimaryKey(autoGenerate = true)
        @NonNull
        var id: Int = 0,
        val language1: String = "",
        val language2: String = ""
)