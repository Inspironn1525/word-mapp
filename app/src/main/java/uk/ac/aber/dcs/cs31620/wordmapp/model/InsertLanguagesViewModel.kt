package uk.ac.aber.dcs.cs31620.wordmapp.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import uk.ac.aber.dcs.cs31620.wordmapp.datasource.WordMappRepository

class InsertLanguagesViewModel(application: Application): AndroidViewModel(application) {

    private val repository: WordMappRepository = WordMappRepository(application)

    fun insertLanguage(language: Language){
        repository.insertLanguage(language)
    }
}