package uk.ac.aber.dcs.cs31620.wordmapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import uk.ac.aber.dcs.cs31620.wordmapp.R
import uk.ac.aber.dcs.cs31620.wordmapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val toolbar = binding.toolbar
        setSupportActionBar(toolbar)

        val navDrawer = binding.drawerLayout
        val toggle = ActionBarDrawerToggle(
            this,
            navDrawer,
            toolbar,
            R.string.nav_open_drawer,
            R.string.nav_close_drawer
        )

        navDrawer.addDrawerListener(toggle)
        toggle.syncState()

        val bottomNavView = binding.bottomNavView
        val navController = findNavController(R.id.nav_host_fragment)
        
        // setup
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_word_list, R.id.navigation_settings))
        setupActionBarWithNavController(navController, appBarConfiguration)
        bottomNavView.setupWithNavController(navController)

    }
}