package uk.ac.aber.dcs.cs31620.wordmapp.ui.settings

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import uk.ac.aber.dcs.cs31620.wordmapp.R
import uk.ac.aber.dcs.cs31620.wordmapp.databinding.FragmentSettingsBinding
import uk.ac.aber.dcs.cs31620.wordmapp.model.InsertLanguagesViewModel
import uk.ac.aber.dcs.cs31620.wordmapp.model.Language

/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SettingsFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentSettingsBinding
    private val insertLanguagesViewModel: InsertLanguagesViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        binding.continueButton.setOnClickListener(this)

        return binding.root
    }

    override fun onClick(v: View?) {

        if (binding.foreignLang.text.isNullOrEmpty() || binding.nativeLang.text.isNullOrEmpty()) {
            Toast.makeText(requireContext(), getString(R.string.missing_data), Toast.LENGTH_SHORT)
                .show()
        } else {
            val language =
                Language(0, binding.nativeLang.text.toString(), binding.foreignLang.text.toString())


            insertLanguagesViewModel.insertLanguage(language)
            findNavController().navigateUp()
        }
    }
}